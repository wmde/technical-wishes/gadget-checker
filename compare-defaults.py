#!/usr/bin/env python3
"""
List wikis where conflicting ReferenceTooltips gadgets are enabled by default.
"""
import csv

config = [
  'nav_popups_gadget',
  'ref_tooltips_gadget',
]

def checkDefaults(name, conflict_map):
  with open('baseline-' + name + '.csv') as f:
    reader = csv.DictReader(f)
    gadget_config = [row for row in reader]

  default_conflicts = []
  for row in gadget_config:
    conflicting_gadget = conflict_map[row['wiki']][name]
    if row['default'] == 'true' and row['gadget'] == conflicting_gadget:
      default_conflicts.append(row['wiki'])

  with open('default_conflicts-' + name + '.csv', "w") as f:
    writer = csv.writer(f)
    writer.writerows([(wiki, ) for wiki in default_conflicts])


def run():
  conflict_map = {}
  with open("gadget_conflict_config.csv") as f:
    reader = csv.DictReader(f)
    for row in reader:
      conflict_map[row['wiki']] = row

    for name in config:
      checkDefaults(name, conflict_map)


if __name__ == '__main__':
  run()
