#!/usr/bin/env python3
"""Parse cached MediaWiki configuration for gadget names.

Usage:
  read-config.py --cached-config=<path>

Options:
  --cached-config=<path>


Create the configuration cache by running this command in the mediawiki-config repository:
  composer run buildConfigCache

Then point this script at the resulting directory:
  read-config.py --cached-config mediawiki-config/tests/data/config-cache
"""
import csv
import docopt
import glob
import json
import os.path
import re


def run():
  arguments = docopt.docopt(__doc__)
  dir_path = arguments['--cached-config']

  result = []
  for path in glob.glob(os.path.expanduser(f'{dir_path}/conf-production-*.json')):
    with open(path) as f:
      m = re.match(r'^.*production-(.*)\.json$', path)
      wiki = m.group(1)
      conf = json.load(f)
      result.append({
        'wiki': wiki,
        'nav_popups_gadget': conf['wgPopupsConflictingNavPopupsGadgetName'],
        'ref_tooltips_gadget': conf['wgPopupsConflictingRefTooltipsGadgetName'],
      })
  
  out_path="gadget_conflict_config.csv"
  print(f"Writing config to {out_path}")
  with open(out_path, "w") as f:
    writer = csv.DictWriter(f, fieldnames=['wiki', 'nav_popups_gadget', 'ref_tooltips_gadget'])
    writer.writeheader()
    writer.writerows(sorted(result, key=lambda row: row['wiki']))


if __name__ == '__main__':
  run()
