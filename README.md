This script pulls a list of active gadgets from every Wikimedia
project and can perform analysis on the results.  Currently used to
check for the actual gadget name for the Reference Tooltips gadget.

Usage
===
Log into an [analytics server](https://wikitech.wikimedia.org/wiki/Analytics/Systems/Clients) to run these scripts.

To get fresh results, simple run

    make

If you want to incrementally build up the results without cleaning up existing files,

    make build
