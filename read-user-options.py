#!/usr/bin/env python3
"""Extract user gadget options directly from the databases.

Should be run from the analytics conda environment:
    source conda-analytics-activate base
"""
import csv
import os
import wmfdata.mariadb
import wmfdata.utils


def match_user_gadget_options(db, gadget):
    option_name=f"gadget-{gadget}"
    return wmfdata.mariadb.run(
        f"""
        select
            database() as wiki,
            count(*) as num_matches
        from user_properties
        where
            up_property like '{option_name}'
            and up_value = '1'
        """,
        db
    )


def read_config():
    with open('gadget_conflict_config.csv') as f:
        reader = csv.DictReader(f)
        return [row for row in reader]


def query_options(config):
    try:
        nav_matches = match_user_gadget_options(config['wiki'], config['nav_popups_gadget'])
        ref_matches = match_user_gadget_options(config['wiki'], config['ref_tooltips_gadget'])
        return {
            'wiki': config['wiki'],
            'users_with_nav_popups': nav_matches.loc[0].at['num_matches'],
            'users_with_ref_tooltips': ref_matches.loc[0].at['num_matches']
        }
    except:
        print(f"Failed to access db {config['wiki']}.")
        return None


def write_output(result):
    out_path="gadget_user_conflicts.csv"
    print(f"Writing results to {out_path}")
    with open(out_path, "w") as f:
        writer = csv.DictWriter(f, fieldnames=['wiki', 'users_with_nav_popups', 'users_with_ref_tooltips'])
        writer.writeheader()
        for row in result:
            if row:
                writer.writerow(row)


def run():
    config = read_config()
    result = [query_options(row) for row in config]
    write_output(result)


if __name__ == '__main__':
    run()
