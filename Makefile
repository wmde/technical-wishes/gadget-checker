# Symlink this directory or customize the value to run locally.
MEDIAWIKI_CONFIG_DIR := /srv/mediawiki-config

CACHED_CONFIG_COPY := mediawiki-config
GENERATED_CONFIG_EXAMPLE := $(CACHED_CONFIG_COPY)/tests/data/config-cache/conf-production-aawiki.json
GADGETS := nav_popups ref_tooltips
BASELINE_FILES := $(patsubst %,baseline-%_gadget.csv,$(GADGETS))
CONFLICTS_FILES := $(patsubst %,default_conflicts-%_gadget.csv,$(GADGETS))

.PHONY: baseline build clean config-cache default_conflicts

all: clean build

build: baseline default_conflicts gadget_conflict_config.csv gadget_user_conflicts.csv

clean:
	rm -rf $(BASELINE_FILES) $(CONFLICTS_FILES) $(CACHED_CONFIG_COPY) gadget_conflict_config.csv gadget_user_conflicts.csv

baseline: $(BASELINE_FILES)

$(BASELINE_FILES):
	python3 gadget-check.py

default_conflicts: $(CONFLICTS_FILES)

$(CONFLICTS_FILES): $(BASELINE_FILES) gadget_conflict_config.csv
	python3 compare-defaults.py

config-cache: $(GENERATED_CONFIG_EXAMPLE)

composer.phar:
	wget https://raw.githubusercontent.com/composer/getcomposer.org/dc44e9ba281837928b2c5be4685c15ee2af04e2f/web/installer -O - -q | php -- --quiet

$(CACHED_CONFIG_COPY)/vendor: composer.phar
	cd $(CACHED_CONFIG_COPY); \
		../composer.phar install \
			--ignore-platform-req=ext-dom \
			--ignore-platform-req=ext-simplexml \
			--ignore-platform-req=ext-xml \
			--ignore-platform-req=ext-xmlwriter

$(GENERATED_CONFIG_EXAMPLE): $(CACHED_CONFIG_COPY) $(CACHED_CONFIG_COPY)/vendor
	cd $(CACHED_CONFIG_COPY); \
		../composer.phar run buildConfigCache

$(CACHED_CONFIG_COPY):
	rsync -av --delete --exclude=.git $(MEDIAWIKI_CONFIG_DIR)/ $(CACHED_CONFIG_COPY)/

gadget_conflict_config.csv: config-cache
	python3 read-config.py --cached-config $(CACHED_CONFIG_COPY)/tests/data/config-cache

gadget_user_conflicts.csv: gadget_conflict_config.csv
	bash -c "source conda-analytics-activate base && \
		python3 read-user-options.py"
