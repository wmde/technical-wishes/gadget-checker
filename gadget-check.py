#!/usr/bin/env python3
"""checks for non-canonical names of gadgets

Usage:
    gadget-check.py [options]

Options:
    -h --help           Show this screen
    -l --limit=<wikis>  Comma-separated list of projects to query
    --concurrency=<num> Maximum number of threads to run  [default: 10]
"""


# LIBRARIES
from concurrent.futures import ThreadPoolExecutor
import docopt
import re  # regex
import csv  # storing in csv file
import requests  # api calls

# VARIABLES
config = {
    'nav_popups_gadget': { 'regex': r'(?i)(Navigation|Popup|NavPopup)', 'snippet':'setupTooltipsLoop' },
    'ref_tooltips_gadget': { 'regex': r'(?i)(Tooltip|\bRef|Cite)', 'snippet':'mouseleave.rt' },
}
fields = ['wiki', 'gadget', 'default', 'script']  # data fields we retrieve
user_agent = 'Gadget Checker 0.1 <wikimedia-de-tech@wikimedia.de>'

headers = {
    'user-agent': user_agent,
}


# FUNCTIONS

# read dbname and API urls of wikis from sitematrix
# @return list of wiki metadata
def getWikis(limit=None):
    result = []
    url = 'https://www.mediawiki.org/w/api.php?action=sitematrix&format=json&smtype=language&smsiteprop=url|dbname&formatversion=2'
    sitematrix = requests.get(url, headers=headers).json()

    for site in sitematrix['sitematrix'].values():
        if type(site) is not int:  # ignore the count field
            if site['site']:
                for entry in site['site']:
                    if limit and entry['dbname'] not in limit:
                        continue
                    result.append((entry['dbname'], entry['url']))

    return result


def fetch_gadgets(link):
    api = link + '/w/api.php?action=query&format=json&list=gadgets&gaprop=id|metadata|desc'
    json = requests.get(api, headers=headers).json()
    return json['query']['gadgets']


# search for gadget in wiki with lookup
# @param link
# @param lookup
def searchGadget(dbname, gadgets, lookup):
    for gadget in gadgets:
        if re.search(lookup, str(gadget)):

            print("  gadget: " + gadget['id'])  # feedback for user

            # get data fields
            default = 'false'
            if "default" in gadget['metadata']['settings']:
                default = 'true'

            if len(gadget['metadata']['module']['scripts']):
                scripts = gadget['metadata']['module']['scripts'][0]
                yield (dbname, gadget['id'], default, scripts)


# use the search api to look for part of the script
# @param link
def searchPage(dbname, link, snippet):
    api = link + '/w/api.php?action=query&format=json&list=search&formatversion=2&srsearch="' + snippet + '"&srnamespace=8'

    json = requests.get(api, headers=headers).json()

    if 'error' in json:
        print("Error in %s: %s" % (dbname, json['error']['info']))
        return

    if not json['query']['searchinfo']['totalhits'] == 0:
        # get metadata from search results and write it to file
        for entry in json['query']['search']:
            canonical_title = 'MediaWiki:' + entry['title'].split(':', 1)[1]
            print("  page: " + canonical_title)
            yield (dbname, '-', '-', canonical_title)


def searchWiki(metadata):
    dbname, api_url = metadata

    print(dbname)

    results = {}

    # use the gadget api
    gadgets = fetch_gadgets(api_url)
    for name, criteria in config.items():
        results[name] = \
            list(searchGadget(dbname, gadgets, criteria['regex'])) + \
            list(searchPage(dbname, api_url, criteria['snippet']))

    return results


def search(limit, concurrency):
    # get urls of all wikis
    wikis = getWikis(limit)

    # iterate over all wikis
    with ThreadPoolExecutor(max_workers=concurrency) as executor:
        results = list(executor.map(searchWiki, wikis))

    #prepare output files to store results line by line
    for name in config:
        outputfile = 'baseline-' + name + '.csv'

        with open(outputfile, 'w') as file:
            csvwriter = csv.writer(file)
            csvwriter.writerow(fields)

            gadget_results = [result[name] for result in results]
            flattened_results = [line for batch in gadget_results for line in batch]
            csvwriter.writerows(sorted(flattened_results))

        print ('Done. Have a look at '+outputfile+' for the results!')

# MAIN

def run():
    arguments = docopt.docopt(__doc__)
    print(arguments)
    if arguments['--limit']:
        limit = arguments['--limit'].split(",")
    else:
        limit = None

    search(limit, int(arguments['--concurrency']))


if __name__ == '__main__':
    run()
